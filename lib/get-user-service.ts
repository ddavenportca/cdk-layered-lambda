import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';

export class getUserService extends Construct {
  constructor(
    scope: Construct,
    id: string,
    sharedLayers: lambda.LayerVersion[]
  ) {
    super(scope, id);

    const handler = new lambda.Function(this, 'getUserHandler', {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset('build/src/functions/get-user'),
      layers: sharedLayers,
      handler: 'handler.handler'
    });

    const api = new apigateway.RestApi(this, 'get-user-api', {
      restApiName: 'Get User Service',
      description: 'Service to get a user.'
    });

    const getUserIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' }
    });

    api.root.addMethod('GET', getUserIntegration);
  }
}
