import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as create_user_service from './create-user-service';
import * as get_user_service from './get-user-service';
import * as delete_user_service from './delete-user-service';

export const APP_NAME_PREFIX = 'CDK-LLS-';

export class CdkLayeredLambdaStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const sharedDependencies = new lambda.LayerVersion(
      this,
      APP_NAME_PREFIX.concat('SharedDependencies'),
      {
        code: lambda.Code.fromAsset('build/', {
          bundling: {
            image: lambda.Runtime.NODEJS_12_X.bundlingImage,
            environment: { CODE_PATH: 'node_modules' },
            command: ['./bundle.sh']
          }
        }),
        compatibleRuntimes: [
          lambda.Runtime.NODEJS_14_X,
          lambda.Runtime.NODEJS_12_X
        ]
      }
    );

    const commonCode = new lambda.LayerVersion(
      this,
      APP_NAME_PREFIX.concat('CommonCode'),
      {
        code: lambda.Code.fromAsset('build/', {
          bundling: {
            image: lambda.Runtime.NODEJS_12_X.bundlingImage,
            environment: { CODE_PATH: 'src/common/*' },
            command: ['./bundle.sh']
          }
        }),
        compatibleRuntimes: [
          lambda.Runtime.NODEJS_14_X,
          lambda.Runtime.NODEJS_12_X
        ]
      }
    );

    new create_user_service.createUserService(
      this,
      APP_NAME_PREFIX.concat('CreateUserService'),
      [sharedDependencies, commonCode]
    );
    new get_user_service.getUserService(
      this,
      APP_NAME_PREFIX.concat('GetUserService'),
      [sharedDependencies, commonCode]
    );
    new delete_user_service.deleteUserService(
      this,
      APP_NAME_PREFIX.concat('DeleteUserService'),
      [sharedDependencies, commonCode]
    );
  }
}
