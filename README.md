# NodeJS Layered Lambda deployed with CDK
This is an example project that creates layered lamdba's using the Cloudformation Development Kit (CDK).
## Requirements

- [NodeJS](https://nodejs.org/en/)
- [nvm](https://github.com/nvm-sh/nvm)
- [CDK v2](https://docs.aws.amazon.com/cdk/v2/guide/home.html)

The CDK assumes that you have your AWS CLI installed and configured to connect to AWS. If you need to use a profile for assuming a role you can use a tool such as [aws-vault](https://github.com/99designs/aws-vault).

## Initial Setup
1. Install the requires npm modules: `npm install`
2. [Bootstrap](https://docs.aws.amazon.com/cdk/v2/guide/bootstrapping.html) your CDK environment: `cdk bootstrap aws://ACCOUNT-NUMBER/REGION`

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npm run deploy`  deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
* `cdk destroy`     *Destroy* this stack


# Project Goal
This project demonstrates working with [Lambdas](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html) written in [Typescript](https://www.typescriptlang.org/). The project allows for lamda code to be tested locally and then be deployed to AWS with shared code layers. Deployments are handles by the CDK.

## Project "app"
This example provides a very simple app with example user functions connected to the [jsonplaceholder mock API](https://jsonplaceholder.typicode.com).

Our app will consist of three functions: get-user, create-user, and delete-user, but we'll only implement get-user.  
`get-user` lambda will call `user-service` which will execute `axios` to get user from a REST endpoint.

- Lambdas: `get-user`, `create-user`, `delete-user`
- Service: `common/services/user-service`
- Dependencies: `aws-cdk` and `axios` as 3rd party libraries

## Special Mappings to support local development
When sharing code using lambda layers, AWS deploys your layer to `/opt/nodejs` in the lambda execution environment. In order to be able to test the code locally we need to add path mapping to our tsconfig.json.

As an example we have `/src/common/service/user-service` which normally we would import to our handler as `import { getUser } from '../../common/service/user-service'`, but since we are deploying `/src/common` as a shared layer at runtime the above import won't resolve. So we have to import it as `/opt/nodejs/service/user-service`. As soon as we do this, VSCode and our local invocation fails because Jest and other local tools can't find `/opt/nodejs`.

To solve these mapping issues we have to sprinkle some special mapping codes in a few places:

### Typescript compiler Mapping `/opt/nodejs` in `tsconfig.json`

Modify `tsconfig.json` and added the following to the `compileOPtions` section

```
    "baseUrl": ".",
    "paths": {
      "/opt/nodejs/*": [
        "src/common/*"
      ]
    }
  },
```

### Jest Mapping

To keep Jest happy we have to add some special mapping during configuration of Jest in `jest.config.js`:

```
  moduleNameMapper: {
    '^/opt/nodejs/(.*)$': '<rootDir>/src/common/$1'
  }
```

### ts-node mapping

We use ts-node to handle local invocation of our lambdas but ts-node can't handle this mapping and relies on `tsconfig-paths` package which we need to install.

### package.json mapping - using tsconfig-paths.

We need to modify our local invocation to use `tsconfig-paths` as follows:

```
    "exec:delete-user": "ts-node -r tsconfig-paths/register src/local/executor.ts ../functions/delete-user/handler delete-user"

```

> Note: that we registered our `executor.ts` with `tsconfig-paths` and now our imports will resolve correctly locally.

---

# Code Structure
```
|-- README.md
|-- bin
|   '-- cdk-layered-lambda.ts
|-- build
|   '-- bundle.sh
|-- cdk.json
|-- jest.config.js
|-- lib
|   |-- cdk-layered-lambda-stack.ts
|   |-- create-user-service.ts
|   |-- delete-user-service.ts
|   '-- get-user-service.ts
|-- package-lock.json
|-- package.json
|-- src
|-- test
|   '-- setup.js
'-- tsconfig.json
```
**Key Paths**
* bin - This location stores the CDK application definition, it roughly equates to a Lambda application in AWS.
* build - This path is using while we build the code bundles used to create the layers in AWS.
* lib - This is where we define hour our Lamdba functions will be deployed to AWS.
* src - Application code (see below).
* test - Setup for our jest tests.

**Key files**
* cdk.json - Produced by running `cdk init`, this file help setup the cdk project.
* jest.config.js - Contains configuration for our unit tests.
* package.json - npm configuration of packages required by the project.
* tsconfig.json - This stores configuration for the Typescript compiler.

## Application code
This project makes use of shared service code to be utilized by the API functions.
```
src
|-- common
|   |-- constants.ts
|   |-- services
|   |   '-- user-service.ts
|   '-- types
|       '-- types.ts
|-- events
|   |-- create-user.ts
|   |-- delete-user.ts
|   '-- get-user.ts
|-- functions
|   |-- create-user
|   |   |-- __test__
|   |   |   |-- handler.test.ts
|   |   |   '-- mocks
|   |   |       '-- create-user-mocks.ts
|   |   '-- handler.ts
|   |-- delete-user
|   |   |-- __test__
|   |   |   |-- handler.test.ts
|   |   |   '-- mocks
|   |   |       '-- delete-user-mocks.ts
|   |   '-- handler.ts
|   '-- get-user
|       |-- __test__
|       |   |-- handler.test.ts
|       |   '-- mocks
|       |       '-- user-mocks.ts
|       '-- handler.ts
|-- index.ts
'-- local
    '-- executor.ts
```
* common - This is the location for our shared services. In an application with a persistence layer or messaging the shared code would go here.
* events - This location contains mock objects to simulate AWS Lambda events for use in local testing.
* functions - The actual Lambda functions
* local - Used for local execution of our functions

# Deployment
Once the code has been coded and tested locally it can be deployed to AWS.
1. Ensure you have the AWS CLI installed and credentials configured to connect to your target environment.
2. If needed export your AWS profile to an environment variable: `export AWS_PROFILE=development`
3. Ensure all node modules are installed: `npm install`
4. Deploy the code to AWS: `npm run deploy`

## Manually test the deployed code
The output of the deploy will list the path's to the API gateway for the three user functions. We can use curl to test them:
* Get user: `curl -X GET `
